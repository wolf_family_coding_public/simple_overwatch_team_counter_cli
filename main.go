package main

import (
	"fmt"
	"os"
	"sort"
	"github.com/alecthomas/kong"
	"gopkg.in/yaml.v3"
	"strconv"
	_ "embed"
	"errors"
)

var CLI struct {
	Team struct {
		Characters []string `arg:"" name:"characters" help:"characters to counter" type:"string"`
	} `cmd:"" help:"counter characters" default:"withargs"`
}

func sortMapStringString(input map[string]string) map[string]string {
	// Convert the map to a slice of key-value pairs
	var kvSlice []struct {
		Key   string
		Value string
	}
	for key, value := range input {
		kvSlice = append(kvSlice, struct {
			Key   string
			Value string
		}{key, value})
	}

	// Define a custom sorting function to sort by values
	sort.Slice(kvSlice, func(i, j int) bool {
		return kvSlice[i].Value < kvSlice[j].Value
	})

	// Create a new map from the sorted slice
	sortedMap := make(map[string]string)
	for _, kv := range kvSlice {
		sortedMap[kv.Key] = kv.Value
	}
	return sortedMap
}

func main() {
	ctx := kong.Parse(&CLI)
	switch ctx.Command() {
	case "team <characters>":
		CounterTeam(CLI.Team.Characters)
	default:
		CounterTeam(CLI.Team.Characters)
	}
}

//go:embed counters.yaml
var charlist []byte

func readChars() map[string][]string {
	data := make(map[string][]string)
	yamlFile, err := os.ReadFile("counters.yaml")
	if errors.Is(err, os.ErrNotExist) {
		yamlFile = []byte(charlist)
	} else if err != nil {
		fmt.Printf("yamlFile.Get err #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, data)
	if err != nil {
		fmt.Printf("Unmarshal: %v", err)
	}
	return data
}

func dup_count(list []string) map[string]int {
	duplicate_frequency := make(map[string]int)
	for _, item := range list {
		_, exist := duplicate_frequency[item]
		if exist {
			duplicate_frequency[item] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency[item] = 1 // else start counting from 1
		}
	}
	return duplicate_frequency
}

func CounterTeam(characters []string) {
	//characters ana -> [dva widow junker tracer], kiriko -> [widow sombra sojourn tracer]
	// need
	// dva -> ana
	// tracer -> ana, kiriko
	counters := map[string][]string{}
	characterMap := readChars()
	for _, character := range characters {
		for _, singleCounterCharacter := range characterMap[character] {
			counters[singleCounterCharacter] = append(counters[singleCounterCharacter], character)
		}
	}
	
	
	// Convert the map to a slice of key-slice pairs
	var kvSlice []struct {
        Key   string
        Value []string
    }

    for key, value := range counters {
        kvSlice = append(kvSlice, struct {
            Key   string
            Value []string
        }{key, value})
    }

    // Define a custom sorting function to sort by the length of the slice
    sort.Slice(kvSlice, func(i, j int) bool {
        return len(kvSlice[i].Value) > len(kvSlice[j].Value)
	})

	for _, counterChar := range kvSlice {
		var stringToPrint string = ""
		var countOfCounters = 0
		for _, charToCounter := range counterChar.Value {
			countOfCounters +=1
			if stringToPrint == "" {
				stringToPrint = charToCounter
			} else {
				stringToPrint += ", " + charToCounter
			}
		}
		fmt.Println(counterChar.Key + " counters " + strconv.Itoa(countOfCounters) + " hero(es): " + stringToPrint)
	}
}
