export GOOS=windows
export GOARCH=amd64
go build -o build/windows/amd64/ow.exe

export GOOS=linux
export GOARCH=amd64
go build -o build/linux/amd64/ow

unset GOOS
unset GOARCH