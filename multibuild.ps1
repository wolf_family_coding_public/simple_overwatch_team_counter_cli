Set-Item -Path Env:GOOS -Value ("windows")
Set-Item -Path Env:GOARCH -Value ("amd64")
go build -o build/windows/amd64/ow.exe

Set-Item -Path Env:GOOS -Value ("linux")
Set-Item -Path Env:GOARCH -Value ("amd64")
go build -o build/linux/amd64/ow

Set-Item -Path Env:GOOS -Value ("")
Set-Item -Path Env:GOARCH -Value ("")