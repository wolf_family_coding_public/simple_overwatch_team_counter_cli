module ow_team_counter

go 1.21.1

require (
	github.com/alecthomas/kong v0.8.1
	gopkg.in/yaml.v3 v3.0.1
)
